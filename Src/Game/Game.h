// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_GAME_GAME_H_

#define SRC_GAME_GAME_H_

#include <atomic>
#include <set>
#include <utility>

#include "Game/Board.h"
#include "Game/GameHelper.h"
#include "Game/Player.h"

namespace EC {

/**
 * @brief The game engine
 */
class Game : public GameHelper {
 public:
    /**
     * @brief Construct a new game
     * @param board Board used to play the game
     * @param player1 First player of the game
     * @param player2 Second player of the game
     */
    Game(const Board& board, Player* player1, Player* player2);

    /**
     * @brief Run the game main loop
     */
    void Run();

    /**
     * @brief Stop the game
     */
    inline void Stop() { run = false; }

    /**
     * @brief Get the list of possible tile that can be captured, playerTiles.size() + opponentTiles.size() must be < to Board::Width * Board::Height
     * @pre playerTiles.size() + opponentTiles.size() < Board::Width * Board::Height
     * @param playerTiles Tiles that are currently captured by the player
     * @param opponentTiles Tiles that are currently captured by the opponent
     * @return The list of tiles that can be captured by the player
     */
    std::set<Position> GetPossibleCapture(std::set<Position> playerTiles,
                                          std::set<Position> opponentTiles) const override;

    /**
     * @brief Return true if the player won the game
     * @param playerTiles Tiles of the player
     * @param opponentTiles Tiles of the opponent
     * @return True if the player won the game
     */
    bool DidIWin(std::set<Position> playerTiles,
                 std::set<Position> opponentTiles) const override;

    Board::Tile GetPlayerTotalElements(std::set<Position> playerTiles) const override;

 private:
    const Board& board;  ///< Board of the game
    Player* player1;  ///< Reference to the first player of the game
    Player* player2;  ///< Reference to the second player of the game
    std::set<Position> player1Tiles;  ///< Tiles captured by the first player
    std::set<Position> player2Tiles;  ///< Tiles captured by the second player

    std::atomic_bool run;  ///< The game must run while run is at true
};

}  // namespace EC

#endif  // SRC_GAME_GAME_H_
