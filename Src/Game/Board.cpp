// Copyright RariumUnlimited - Licence : MIT
#include "Game/Board.h"

#include <chrono>
#include <random>

namespace EC {

Board::Board() {
    using UIntDistrib = std::uniform_int_distribution<unsigned int>;
    std::mt19937 generator(static_cast<unsigned int>(
                    std::chrono::system_clock::now().time_since_epoch().count()));
    UIntDistrib distrib(0, MaxElementAmount);
    for (auto i = 0; i < Width * Height; ++i) {
        tiles[i].Fire = distrib(generator);
        tiles[i].Water = distrib(generator);
        tiles[i].Wind = distrib(generator);
        tiles[i].Earth = distrib(generator);
        tiles[i].Metal = distrib(generator);
    }
}

}  // namespace EC
