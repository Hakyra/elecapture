// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_GAME_PLAYER_H_

#define SRC_GAME_PLAYER_H_

#include <utility>
#include <set>

#include "Game/Board.h"
#include "Game/GameHelper.h"

namespace EC {

/**
 * @brief A player of the game
 */
class Player {
 public:
    virtual ~Player() {}

    using Position = std::pair<unsigned int, unsigned int>;
    /**
     * @brief TakeTurn Player must take its turn and return the position of the tile he wants to capture
     * @param b The board of the game
     * @param helper Game helper functions that can be of use by the player
     * @param possibleCapture List of tile that the player can capture during the turn
     * @param yourTiles The tiles already captured by the player
     * @param opponentTiles The tiles already captured by the opponent
     * @return The position of the tile that the player wants to capture
     */
    virtual Position TakeTurn(const Board& b,
                              const GameHelper* helper,
                              const std::set<Position>& possibleCapture,
                              const std::set<Position>& yourTiles,
                              const std::set<Position>& opponentTiles) = 0;

    /**
     * @brief Notify the player that tiles captured changed, will be called during Game constructor before any player takes a turn and each time any player takes a turn afterwards
     * @param helper Game helper functions that can be of use by the player
     * @param yourTiles The player tiles
     * @param opponentTiles The opponent tiles
     */
    virtual void NotifyCapture(const GameHelper* helper,
                               const std::set<Position>& yourTiles,
                               const std::set<Position>& opponentTiles) {}

    /**
     * @brief Notify the player that the game ended
     * @param helper Game helper functions that can be of use by the player
     * @param win True if the player won the game
     */
    virtual void NotifyGameEnd(const GameHelper* helper, bool win) {}
};

}  // namespace EC

#endif  // SRC_GAME_PLAYER_H_
