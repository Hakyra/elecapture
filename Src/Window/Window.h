// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_WINDOW_H_

#define SRC_WINDOW_WINDOW_H_

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <set>

#include <Rarium/Window/Window.h>
#include <SFML/Graphics.hpp>

#include "Game/Board.h"
#include "Game/Player.h"

namespace EC {

/**
 * @brief A window where we will this our game
 */
class Window : public RR::Window, public Player {
 public:
    /**
     * @brief Construct and open a new window
     */
    explicit Window(const Board& board);

    /**
     * @brief Called when the window is closed
     */
    void OnClose();

    /**
     * @brief Main loop iteration, draw things, ...
     */
    void Draw(float delta, sf::RenderTarget& target) override;

    Position TakeTurn(const Board& b,
                      const GameHelper* helper,
                      const std::set<Position>& possibleCapture,
                      const std::set<Position>& yourTiles,
                      const std::set<Position>& opponentTiles) override;

    void NotifyCapture(const GameHelper* helper,
                       const std::set<Position>& yourTiles,
                       const std::set<Position>& opponentTiles) override;

    void NotifyGameEnd(const GameHelper* helper, bool win) override;

    static constexpr unsigned int TileSize = 64;  ///< Size in pixel of a tile
    static constexpr unsigned int Width = Board::Width * TileSize;  ///< Width of the window in pixel
    static constexpr unsigned int Height = Board::Height * TileSize;  ///< Height of the window in pixel

 private:
    using SpriteTiles = std::array<sf::Sprite, Board::Width * Board::Height>;
    using TextTiles = std::array<sf::Text, Board::Width * Board::Height>;

    /// Get the sprite of a tile of the board
    inline sf::Sprite& TSprite(unsigned int x, unsigned int y) {
        assert(x >= 0 && y >= 0 && x < Board::Width && y < Board::Height);
        return stiles[x + y * Board::Width];
    }

    /// Get the text of a tile of the board
    inline sf::Text& TText(unsigned int x, unsigned int y) {
        assert(x >= 0 && y >= 0 && x < Board::Width && y < Board::Height);
        return ttiles[x + y * Board::Width];
    }

    /// Get the texture corresponding to a tile : the one corresponding to the element in most abondance on the tile
    sf::Texture& GetTileTexture(const Board::Tile& t);

    /// Display status of the game : how many of each elements the player and IA have
    void DisplayStatus(const GameHelper* helper);

    void MouseClick(const RR::InputState& state, sf::Mouse::Button);

    const Board& board;  ///< Board of the game
    SpriteTiles stiles;  ///< Sprites for each tiles of the board
    TextTiles ttiles;  ///< Texts for each tiles of the board

    std::mutex capturedTilesMutex;  ///< Mutex used when accessing captured/oppenent tiles
    std::set<Position> capturedTiles;  ///< Tiles that the user captured
    std::set<Position> opponentTiles;  ///< Tiles that the opponent captured

    static sf::Font Font;  ///< Font used to draw text
    sf::Texture fireTexture;  ///< Texture for a tile that have a majority of fire element
    sf::Texture waterTexture;  ///< Texture for a tile that have a majority of water element
    sf::Texture windTexture;  ///< Texture for a tile that have a majority of wind element
    sf::Texture earthTexture;  ///< Texture for a tile that have a majority of earth element
    sf::Texture metalTexture;  ///< Texture for a tile that have a majority of metal element

    std::atomic_bool playerTurn;  ///< Set to true if it is the player turn

    std::mutex cvMutex;  ///< Mutex locked when using cvTurn
    std::condition_variable cvTurn;  ///< Condition variable used to get the tile the player clicked when waiting for him to take his turn
    Position playedPosition;  ///<
};

}  // namespace EC

#endif  // SRC_WINDOW_WINDOW_H_
