// Copyright RariumUnlimited - Licence : MIT
#include "Window/Window.h"

#include <Rarium/Tools/Log.h>

#include <sstream>
#include <system_error>

namespace EC {

Window::Window(const Board& board) :
    RR::Window("Elemental Capture",
               sf::VideoMode(Width, Height),
               sf::Style::Close),
    board(board),
    playerTurn(false) {
    Font.loadFromFile("./Assets/arial.ttf");
    fireTexture.loadFromFile("./Assets/Fire.png");
    waterTexture.loadFromFile("./Assets/Water.png");
    windTexture.loadFromFile("./Assets/Wind.png");
    earthTexture.loadFromFile("./Assets/Earth.png");
    metalTexture.loadFromFile("./Assets/Metal.png");

    MouseButtonPressedEvent += new RR::MouseButtonEvent::T<Window>(this, &Window::MouseClick);

    for (auto j = 0; j < Board::Height; ++j) {
        for (auto i = 0; i < Board::Width; ++i) {
            const Board::Tile& t = board.T(i, j);
            sf::Sprite& sprite = TSprite(i, j);
            sprite.setTexture(GetTileTexture(t));
            sprite.setPosition(i * TileSize, j * TileSize);
            sf::Text& text = TText(i, j);
            text.setPosition(i * TileSize + 2, j * TileSize);
            text.setCharacterSize(11);
            text.setFont(Font);
            text.setOutlineThickness(1.f);
            std::ostringstream oss;
            oss << "Fire: " << t.Fire << "\n" <<
                   "Water: " << t.Water << "\n" <<
                   "Wind: " << t.Wind << "\n" <<
                   "Earth: " << t.Earth << "\n" <<
                   "Metal: " << t.Metal;
            text.setString(oss.str());
        }
    }
}

void Window::Draw(float delta, sf::RenderTarget& target) {
    for (auto j = 0; j < Board::Height; ++j) {
        for (auto i = 0; i < Board::Width; ++i) {
            target.draw(TSprite(i, j));

            if (GetCurrentInput().MousePosition.x >= i * TileSize &&
                GetCurrentInput().MousePosition.x <= (i + 1) * TileSize &&
                GetCurrentInput().MousePosition.y >= j * TileSize &&
                GetCurrentInput().MousePosition.y <= (j + 1) * TileSize) {
                sf::Text& text = TText(i, j);

                if (playerTurn) {
                    text.setFillColor(sf::Color::White);
                    text.setOutlineColor(sf::Color::Black);
                } else {
                    text.setFillColor(sf::Color::Black);
                    text.setOutlineColor(sf::Color::White);
                }
                target.draw(text);
            }
        }
    }

    sf::RectangleShape rect({static_cast<float>(TileSize), 
                             static_cast<float>(TileSize) });

    sf::Color playerColor = sf::Color::Red;
    playerColor.a = 255 * 0.5;
    sf::Color opponentColor = sf::Color::Black;
    opponentColor.a = 255 * 0.5;

    std::unique_lock lock(capturedTilesMutex);
    rect.setFillColor(playerColor);
    for (const Position& p : capturedTiles) {
        rect.setPosition(p.first * TileSize, p.second * TileSize);
        target.draw(rect);
    }

    rect.setFillColor(opponentColor);
    for (const Position& p : opponentTiles) {
        rect.setPosition(p.first * TileSize, p.second * TileSize);
        target.draw(rect);
    }
    lock.unlock();
}

Player::Position Window::TakeTurn(const Board& b,
                                  const GameHelper* helper,
                                  const std::set<Position>& possibleCapture,
                                  const std::set<Position>& yourTiles,
                                  const std::set<Position>& opponentTiles) {
    std::unique_lock lock(capturedTilesMutex);
    capturedTiles = yourTiles;
    this->opponentTiles = opponentTiles;
    lock.unlock();

    RR::Logger::Log() << "Your turn !\n";
    DisplayStatus(helper);

    playerTurn = true;

    std::unique_lock cvLock(cvMutex);
    cvTurn.wait(cvLock);

    playerTurn = false;

    return playedPosition;
}

void Window::NotifyCapture(const GameHelper* helper,
                           const std::set<Position>& yourTiles,
                           const std::set<Position>& opponentTiles) {
    std::unique_lock lock(capturedTilesMutex);
    capturedTiles = yourTiles;
    this->opponentTiles = opponentTiles;
    lock.unlock();
}

void Window::NotifyGameEnd(const GameHelper* helper, bool win) {
    if (win)
        RR::Logger::Log() << "You have won !\n";
    else
        RR::Logger::Log() << "You have lost !\n";

    DisplayStatus(helper);
}

void Window::DisplayStatus(const GameHelper* helper) {
    Board::Tile tp = helper->GetPlayerTotalElements(capturedTiles);
    Board::Tile to = helper->GetPlayerTotalElements(opponentTiles);

    RR::Logger::Log() << "Elements: yours/opponent\n" <<
                         "Fire: " << tp.Fire << "/" << to.Fire << "\n" <<
                         "Water: " << tp.Water << "/" << to.Water << "\n" <<
                         "Wind: " << tp.Wind << "/" << to.Wind << "\n" <<
                         "Earth: " << tp.Earth << "/" << to.Earth << "\n" <<
                         "Metal: " << tp.Metal << "/" << to.Metal << "\n";
}

sf::Texture& Window::GetTileTexture(const Board::Tile& t) {
    if (t.Fire > t.Water &&
        t.Fire > t.Wind &&
        t.Fire > t.Earth &&
        t.Fire > t.Metal)
        return fireTexture;
    else if (t.Water > t.Fire &&
             t.Water > t.Wind &&
             t.Water > t.Earth &&
             t.Water > t.Metal)
        return waterTexture;
    else if (t.Wind > t.Fire &&
             t.Wind > t.Water &&
             t.Wind > t.Earth &&
             t.Wind > t.Metal)
        return windTexture;
    else if (t.Earth > t.Fire &&
             t.Earth > t.Water &&
             t.Earth > t.Wind &&
             t.Earth > t.Metal)
        return earthTexture;
    else
        return metalTexture;
}

void Window::MouseClick(const RR::InputState& state, sf::Mouse::Button) {
    playedPosition.first = state.MousePosition.x / TileSize;
    playedPosition.second = state.MousePosition.y / TileSize;

    cvTurn.notify_one();
}

void Window::OnClose() {
    cvTurn.notify_one();
}

sf::Font Window::Font;

}  // namespace EC
