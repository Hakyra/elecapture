// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_IA_RANDOM_H_

#define SRC_IA_RANDOM_H_

#include <random>
#include <set>

#include "Game/Player.h"

namespace EC {

/**
 * @brief A random IA
 */
class Random : public Player {
 public:
    /**
     * @brief Construct a new random IA
     */
    Random();

    /**
     * @brief TakeTurn Will randomly choose a tile to capture
     * @param b The board of the game
     * @param possibleCapture List of tile that the player can capture during the turn
     * @param yourTiles The tiles already captured by the player
     * @param opponentTiles The tiles already captured by the opponent
     * @return The position of the tile that the player wants to capture
     */
    Position TakeTurn(const Board& b,
                      const GameHelper* helper,
                      const std::set<Position>& possibleCapture,
                      const std::set<Position>& yourTiles,
                      const std::set<Position>& opponentTiles) override;

 private:
    std::mt19937 generator;  ///< Pseudo random number generator for this IA
};

}  // namespace EC

#endif  // SRC_IA_RANDOM_H_
